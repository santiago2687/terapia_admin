@extends('layouts.home-layout')
@section('content')
    	
<div id="container" class="box-body">

    <div class="form-group row">
      <div class="col-xs-12">
        <div style="margin-bottom: 5%;" class="col-xs-10">
          <h3 style="margin-top:0px;" class="h3-detail-pedido">Administrador de Usuarios</h3>
        </div>
      </div>
    </div>

	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#lista_usuarios">Lista Usuarios</a></li>
	    <li><a data-toggle="tab" href="#crear_usuario">Crear Usuario</a></li>
	</ul>

    <div class="tab-content">

        <div id="lista_usuarios" class="tab-pane fade in active">
            
        </div>


        <div id="crear_usuario" class="tab-pane fade">


        	<div id="crear_usuario" class="tab-pane fade" style="opacity:10;">

		        <div id="creacion_usuario_div" class="tab-pane fade in active">
		            <form id="login-form" action="{!! url('user/createUser'); !!}" method="post" role="form" style="display: block;">
		                
		                <div class="box box-primary col-xs-12">

		                      <div class="col-xs-6">

		                        <div class="form-group row margin-first-row-detail-pedido">
		                          <label for="example-text-input" class="col-xs-3 col-form-label detail-order-label">Nombre</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="text" value="" id="first_name" name="first_name">
		                          </div>
		                        </div>

		                        <div class="form-group row">
		                          <label for="" class="col-xs-3 col-form-label detail-order-label">Apellido</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="text" value="" id="last_name" name="last_name">
		                          </div>
		                        </div>

		                        <div class="form-group row">
		                          <label for="" class="col-xs-3 col-form-label detail-order-label">Especialidad</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="text" value="" id="specialty" name="specialty">
		                          </div>
		                        </div>

		                        <div class="form-group row margin-first-row-detail-pedido">
		                          <label for="example-text-input" class="col-xs-3 col-form-label detail-order-label">Matricula Nac.</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="text" value="" id="matricula_nac" name="matricula_nac">
		                          </div>
		                        </div>

		                        <div class="form-group row margin-first-row-detail-pedido">
		                          <label for="example-text-input" class="col-xs-3 col-form-label detail-order-label">Matricula Prov.</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="text" value="" id="matricula_prov" name="matricula_prov">
		                          </div>
		                        </div>

		                        <div class="form-group row">
		                          <label for="" class="col-xs-3 col-form-label detail-order-label">Email</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" placeholder="example@example.com" type="email" value="" id="email" name="email">
		                          </div>
		                        </div>

		                        <div class="form-group row">
		                          <label for="" class="col-xs-3 col-form-label detail-order-label">Contraseña</label>
		                          <div class="col-xs-9">
		                            <input  class="form-control" type="password" value="" id="password" name="password">
		                          </div>
		                        </div>

		                        <div class="form-group row">
		                          <label for="" class="col-xs-3 col-form-label detail-order-label">Grupo</label>
		                          <div class="col-xs-9">
		                            <select class="form-control" id="role" name="role">
		                                <option id="0">Seleccione un grupo...</option>
		                                @foreach ($data['user_groups'] as $group)
		                                    <option id="{{ $group->id }}" value="{{ $group->id }}">{{ $group->group_name }}</option>
		                                @endforeach
		                            </select>
		                          </div>
		                        </div>

		                        <div class="form-group">
		                            <div class="row">
		                                <div class="col-sm-9">
		                                    <input type="submit" name="create_user" id="create-submit" tabindex="4" class="form-control btn btn-block" style="background-color: #008546; color:#fff;" value="Registrar">
		                                </div>
		                            </div>
		                        </div>
		        
		                      </div>
		            
		                </div>
		                <input type="hidden" name="_token" value="{{ csrf_token() }}">
		            </form>

	      	</div>


        </div>


	</div>


 </div>


@section('page-script')

<script type="text/javascript">
$(function() {

	
});
</script>
@stop



@stop
