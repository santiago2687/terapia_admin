<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;

class PacientesController extends Controller
{
    
	public function _constructor() {
		$this->middleware('auth');

	}

	public function getIndex() {
		//$cant_productos = Tp_adic::getCantProductos();
		$pacientes = array();
		return View::make('pages.pacientes.pacientes_index')->with('pacientes', $pacientes);

	}

}
