<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('user/createSimpleUser' , 'UserController@createSimpleUser');


Route::group(['middleware' => ['web']], function () {
     
    Route::get('/', ['middleware'=> 'auth', 'uses' => 'HomeController@getIndex']);
    Route::get('home', ['middleware'=> ['auth'], 'uses' => 'HomeController@getIndex']);

    //Pacientes...
    Route::get('/pacientes', ['middleware'=> ['auth'], 'uses' => 'PacientesController@getIndex']); 


    Route::get('login', 'AuthController@getLogin');
    Route::post('login', 'AuthController@postLogin');
    Route::get('logout', 'AuthController@getLogout');

    // Users Administrator Routes...
    Route::get('/adminUsuarios', ['middleware'=> ['auth'], 'uses' => 'UserController@getIndex']);

    Route::get('user/register', ['middleware'=> ['auth'], 'uses' => 'UserController@register']);

    Route::post('user/createUser', ['middleware'=> ['auth'], 'uses' => 'UserController@createUser']);


    
});