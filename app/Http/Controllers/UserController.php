<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use App\models\User;
use App\models\UsuarioGrupos;
use DB;

class UserController extends Controller
{

    public function _constructor() {
		$this->middleware('auth');

	}

	public function getIndex() {
        $user_groups = UsuarioGrupos::listUserGroup();
        $data['user_groups'] = $user_groups;
		return view::make('pages.auth.user_admin_index')->with('data', $data);
	}

    public function createUser(Request $request) {

    	$exist_user_email = User::validateUserEmail($request->input("email"));

    	$user_groups = UsuarioGrupos::listUserGroup();
    	
    	if ($exist_user_email == "true") {
    		$new_user = User::create([
		        'first_name' => $request->input("first_name"),
		        'last_name' => $request->input("last_name"),
                'specialty' => $request->input("specialty"),
                'mat_nac' => $request->input("matricula_nac"),
                'mat_prov' => $request->input("matricula_prov"),
		        'email' => $request->input("email"),
		        'password' => bcrypt($request->input("password")),
		        'area' => (int)$request->input("area"),
    		]);

    		$msg = "Se ha creado el usuario correctamente!";

    		$data = array('user_groups' => $user_groups, 'msg' => $msg, 'result' => "true");
    		
    		return View::make('pages.auth.user_admin_index')->with('data', $data);

    	} else {

    		$msg = "Ya existe el mail seleccionado";

    		$data = array('user_groups' => $user_groups, 'msg' => $msg, 'result' => "false");

    		return View::make('pages.auth.auth.user_admin_index')->with('data', $data);

    	}


    }

    public function createSimpleUser() {

    	$new_user = User::create([
		        'first_name' => 'Santiago',
		        'last_name' => 'Bermudez',
                'mat_nac' => '123456',
                'mat_prov' => '123456',
                'specialty' => 'Administrador',
		        'email' => 'admin@pairossien.gov.ar',
		        'password' => bcrypt('admin123456'),
		        'area' => 1,
    		]);

    }

        
}