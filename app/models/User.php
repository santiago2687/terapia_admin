<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use DB;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $table = 'users';

    protected $fillable = ['first_name', 'last_name', 'mat_nac', 'mat_prov', 'specialty', 'email', 'password', 'area'];

    public static function getRoleUser($email = null) {
    	if (!is_null($email)) {
    		$role = DB::table('users')->select('area')->Where('email', $email)->get();
	    	return $role;

    	}

    } 

    public static function validateUserEmail($email = null) {
    	if (!is_null($email)) {
    		$email = DB::table('users')->select('email')->Where('email', $email)->get();

    		if (count($email) > 0) {
    			return "false";

    		} else {
    			return "true";

    		}
	    	

    	}
    }
}
