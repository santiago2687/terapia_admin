@extends('layouts.home-layout')
@section('content')

    <div class="form-group row">
      <div class="col-xs-12">
        <div style="margin-bottom: 5%;" class="col-xs-10">
          <h3 style="margin-top:0px;" class="h3-detail-pedido">Pacientes</h3>
        </div>
      </div>
    </div>

   	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" id="lista_pacientes" href="#lista_pacientes">Listado de pacientes</a></li>
	    <li><a data-toggle="tab" id="alta_paciente" href="#alta_paciente">Alta Paciente</a></li>
      <li><a data-toggle="tab" id="alta_ingreso" href="#alta_ingreso">Info de Ingreso</a></li>
	</ul>

    <div class="tab-content">

        <div id="lista_pacientes" class="tab-pane fade in active">
            
        </div>

        <form>

            <div id="alta_paciente" class="tab-pane fade in active">
            
            </div>

            <div id="alta_ingreso" class="tab-pane fade in active">

              <ul class="nav nav-tabs tabs_ingreso" style="margin-top: 2%;">
                <li class="active"><a data-toggle="tab" id="ingreso_general" href="#ingreso_general">General</a></li>
                <li><a data-toggle="tab" id="ingreso_fisico" href="#ingreso_fisico">Examen Físico</a></li>
                <li><a data-toggle="tab" id="ingreso_complementario" href="#ingreso_complementario">Estudios Complementarios</a></li>
                <li><a data-toggle="tab" id="ingreso_indicaciones" href="#ingreso_indicaciones">Indicaciones</a></li>
              </ul>

                <div id="ingreso_general" class="tab-pane fade in active">
                  
                </div>

                <div id="ingreso_complementario" class="tab-pane fade in active">
                  
                </div>

                <div id="ingreso_fisico" class="tab-pane fade in active">
                  
                </div>

                <div id="ingreso_indicaciones" class="tab-pane fade in active">
                  
                </div>


            
            </div>
          
        </form>

        


    </div>


	</div>


@section('page-script')

<script type="text/javascript">
$(function() {

  $('.tabs_ingreso').css('display', 'none');

  $('#lista_pacientes').on('click', function(){

    $('.tabs_ingreso').css('display', 'none');

  });

  $('#alta_paciente').on('click', function(){

    $('.tabs_ingreso').css('display', 'none');

  });

  $('#alta_ingreso').on('click', function(){

    $('.tabs_ingreso').css('display', 'block');

  });

  
});
</script>
@stop


@stop