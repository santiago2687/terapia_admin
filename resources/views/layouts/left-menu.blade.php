
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <!--<div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('assets/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>-->
        <!--<div class="pull-left info">
          <p>Alexander Pierce</p>-->
          <!-- Status -->
          <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>-->
      <!--</div>-->

      <!-- search form (Optional) -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{!! url('/'); !!}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
        <li class="line bg-light"></li>
      </ul>

      <ul class="sidebar-menu">
        <li><a href="{!! url('/pacientes'); !!}"><i class="fa fa-users"></i> <span>Pacientes</span></a></li>
        <li><a href="{!! url('/adminUsuarios'); !!}"><i class="fa fa-user"></i> <span>Adm Usuarios</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
