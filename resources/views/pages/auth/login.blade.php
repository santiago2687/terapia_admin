@extends('layouts.login')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div>
                            <div style="background-color:#008546;" class="col-xs-12">
                            <img src="{{ asset('assets/img/logo_matanza_trans.png') }}">
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="login-form" action="{!! url('/login'); !!}" method="post" role="form" style="display: block;">
                                    <div class="form-group">
                                        <input type="text" name="matricula_nac" id="matricula_nac" tabindex="1" class="form-control" placeholder="Matricula Nacional" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Contraseña">
                                    </div>
                                    <div class="form-group text-center">
                                        <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                        <label for="remember"> Recordarme </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>